from airflow import DAG
from airflow.decorators import task
import json
import requests
from datetime import datetime
from dtp_model import AccidentList
from airflow.operators.python import get_current_context
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry
from airflow.providers.mongo.hooks.mongo import MongoHook

@task()
def get_latest_okato_reg_codes():
    rf_dict = {"maptype":1,"region":"877","pok":"1"}
    r = requests.post("http://stat.gibdd.ru/map/getMainMapData", json=rf_dict)
    content = json.loads(json.loads(json.loads(r.content)["metabase"])[0]['maps'])
    for region in content:
        del region['path']
    return content

@task()
def get_latest_reg_mun_names(okato_reg_codes):
    region_dict = {"maptype": 1, "pok": "1"}
    for region in okato_reg_codes:
        print(region)
        region_dict["region"] = region["id"]
        r = requests.post("http://stat.gibdd.ru/map/getMainMapData", json=region_dict)
        content = (json.loads(json.loads(json.loads(r.content)['metabase'])[0]['maps']))
        region['muns'] = [{"id": mun["id"], "name": mun['name']} for mun in content]
    return okato_reg_codes

def get_cards_by_month_reg_mun_ids(month: list, reg_dict: dict, mun_dict: dict, session: requests.Session):
    cards_request_dict = {
        "data": {
            "date": month,
            "ParReg": reg_dict['id'],
            "order": {
                "type":"1",
                "fieldName":"dat"
            },
            "reg": mun_dict['id'],
            "ind":"1",
            "st":"1",
            "en":"100"
        }
    }
    start = 1
    increment = 100
    r = requests.Response()
    json_data = []
    while True:
        cards_request_dict["data"]["st"] = str(start)
        cards_request_dict["data"]["en"] = str(start + increment - 1)
        cards_request_dict_json = {}

        cards_request_dict_json["data"] = json.dumps(cards_request_dict["data"], separators=(',', ':')).encode('utf8').decode('unicode-escape')

        r = session.post("http://stat.gibdd.ru/map/getDTPCardData", json=cards_request_dict_json)
        if r.status_code == 200:
            try:
                cards = json.loads(json.loads(r.content)["data"])["tab"]
            except:
                break
            if len(cards) > 0:
                for card in cards:
                    card['region_name'] = reg_dict['name']
                    card['mun_name'] = mun_dict['name']
                json_data += cards
            if len(cards) == increment:
                start += increment
            else:
                break
        else:
            break
    return json_data

@task(map_index_template="{{ my_custom_map_index }}", retries=5)
def get_all_cards_by_year(latest_reg_mun_names, year, **kwargs):
    context = get_current_context()
    context["my_custom_map_index"] = f"year: {year}"

    session = requests.Session()

    retries = Retry(total=10, backoff_factor=0.1)
    session.mount('http://', HTTPAdapter(max_retries=retries))
    session.mount('https://', HTTPAdapter(max_retries=retries))

    dates = [f"MONTHS:{month}.{year}" for month in range(1, 13, 1)]
    all_cards = AccidentList([])
    for index, region in enumerate(latest_reg_mun_names):
        reg_cards = []
        for mun in region['muns']:
            cards = get_cards_by_month_reg_mun_ids(dates, region, mun, session)
            reg_cards.extend(cards)
        all_cards.extend(reg_cards)
        print(f"{index}) Successfully exported {len(reg_cards)} for {region['name']}")
    return all_cards.get()

@task
def insert_to_mongo(accidents_list):
    mongo_hook = MongoHook("MongoDB")
    for accidents_by_year in accidents_list:
        mongo_hook.insert_many(mongo_collection="accidents", docs=accidents_by_year)

with DAG(
        dag_id="export_accidents",
        schedule_interval=None,
        start_date=datetime.now(),
        schedule="@once",
        max_active_runs=1
    ) as dag:
    
    latest_okato_reg_codes = get_latest_okato_reg_codes()
    latest_reg_mun_names = get_latest_reg_mun_names(latest_okato_reg_codes)
    insert_to_mongo(get_all_cards_by_year.partial(latest_reg_mun_names=latest_reg_mun_names).expand(year=[str(x)for x in range(2015, 2025)]))
