from airflow import DAG
from airflow.decorators import task
from datetime import datetime
import requests
from airflow.providers.mongo.hooks.mongo import MongoHook
import pandas as pd
import geopandas as gpd
from sklearn.cluster import OPTICS
import json
import numpy as np

@task()
def get_latest_okato_reg_codes():
    rf_dict = {"maptype":1,"region":"877","pok":"1"}
    r = requests.post("http://stat.gibdd.ru/map/getMainMapData", json=rf_dict)
    content = json.loads(json.loads(json.loads(r.content)["metabase"])[0]['maps'])
    for region in content:
        del region['path']
    return content

def export_accidents(region_name):
    mongo_hook = MongoHook("MongoDB")
    next_month = datetime.now().month + 1
    required_months = [
        next_month - 1 if next_month != 1 else 12,
        next_month,
        next_month + 1 if next_month != 12 else 1,

    ]
    agg_pipeline = [
        {
            "$addFields": {"month": {"$month": '$datetime'}}
        },
        {
            "$match": {
                "region_name": region_name,
                "datetime" : {"$gte": datetime.now().replace(year=datetime.now().year - 3)},
                "month": {"$in": required_months},
                "type": {"$in": [
                    "Столкновение",
                    "Наезд на пешехода",
                    "Съезд с дороги",
                    "Опрокидывание",
                    "Наезд на велосипедиста"
                ]}
            }
        }  
    ]
    return pd.DataFrame(list(mongo_hook.aggregate(mongo_collection="accidents", aggregate_query=agg_pipeline)))

@task()
def predict_clusters(latest_okato_reg_codes: list[dict]):
    optics_clusterer = OPTICS(max_eps=300)
    for region in latest_okato_reg_codes:
        print(f"Predicting clusters for region: {region['name']}")
        accidents_df = export_accidents(region_name=region['name'])
        accidents_df = accidents_df.loc[(accidents_df.latitude != '') & (accidents_df.longitude != '')]
        accidents_df = accidents_df.loc[(accidents_df.latitude != '.') & (accidents_df.longitude != '.')]
        accidents_df['longitude'] = pd.to_numeric(accidents_df['longitude'], errors='coerce')
        accidents_df['latitude'] = pd.to_numeric(accidents_df['latitude'], errors='coerce')
        accidents_df = accidents_df.dropna(subset=['longitude', 'latitude'])
        accidents_df = accidents_df.loc[(accidents_df.latitude != 0) & (accidents_df.longitude != 0)]

        accidents_gdf = gpd.GeoDataFrame(accidents_df, geometry=gpd.points_from_xy(x=accidents_df.longitude, y=accidents_df.latitude), crs="EPSG:4326")
        
        accidents_gdf['utm_geometry'] = accidents_gdf['geometry'].to_crs(accidents_gdf.estimate_utm_crs())
        accidents_gdf['utm_longitude'] = accidents_gdf['utm_geometry'].x
        accidents_gdf['utm_latitude'] = accidents_gdf['utm_geometry'].y

        accidents_gdf['utm_latitude'] = accidents_gdf['utm_latitude'].replace(np.inf, None)
        accidents_gdf['utm_longitude'] = accidents_gdf['utm_longitude'].replace(np.inf, None)
        accidents_gdf = accidents_gdf.dropna(subset=['utm_latitude', 'utm_longitude'])

        accidents_gdf['cluster'] = optics_clusterer.fit_predict(accidents_gdf[['utm_longitude', 'utm_latitude']])
        accidents_gdf = accidents_gdf.loc[accidents_gdf.cluster != -1]
        if len(accidents_gdf) == 0:
            print(f"No clusters for {region['name']} were predicted, try to fix model parameters")
            continue
        clusters_geojson = gpd.GeoDataFrame(accidents_gdf.groupby('cluster').apply(lambda x: x.dissolve().convex_hull).reset_index(), geometry=0).to_json()
        result_document = {
            "date": datetime.now(),
            "predicted_month": datetime(datetime.now().year, datetime.now().month, day=1),
            "clusters_geojson": json.loads(clusters_geojson)
        }
        print(result_document)

with DAG(
        dag_id="predict_clusters",
        schedule_interval=None,
        start_date=datetime.now(),
        schedule="@monthly",
        max_active_runs=1
    ) as dag:

    latest_okato_reg_codes = get_latest_okato_reg_codes()
    predict_clusters(latest_okato_reg_codes)