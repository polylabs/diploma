from datetime import datetime
import pandas as pd

class Accident():
    def __init__(self, content: dict):
        self.region_name: str = content['region_name']
        self.mun_name: str = content['mun_name']
        self.datetime: datetime = datetime.strptime(content['date'] + ' ' + content['Time'], "%d.%m.%Y %H:%M")
        self.type: str = content['DTP_V']
        self.dead_number: int = content['POG']
        self.injured_number: int = content['RAN']
        self.car_number: int = content['K_TS']
        self.member_number: int = content['K_UCH']
        self.road_network_disadvantages: list[str] = content['infoDtp']['ndu'] # Недостатки транспортно-эксплуатационного содержания улично-дорожной сети
        self.driving_mode_factors: list[str] = content['infoDtp']['factor']
        self.weather: list[str] = content['infoDtp']['s_pog']
        self.roadway_condition: str = content['infoDtp']['s_pch']
        self.lighting: str = content['infoDtp']['osv']
        self.longitude: float = content['infoDtp']['COORD_L']
        self.latitude: float = content['infoDtp']['COORD_W']
        
        direct_traffic_violations_lists = [card['ts_uch'][0]['NPDD'] for card in content['infoDtp']['ts_info'] if len(card['ts_uch']) != 0]
        direct_traffic_violations_lists: list = list(set(sum(direct_traffic_violations_lists, [])))
        if len(direct_traffic_violations_lists) > 1 and "Нет нарушений" in direct_traffic_violations_lists:
            direct_traffic_violations_lists.remove("Нет нарушений")
        self.direct_traffic_violations = direct_traffic_violations_lists

    def get(self) -> dict:
        return {
            "region_name": self.region_name,
            "mun_name": self.mun_name,
            "datetime": self.datetime,
            "type": self.type,
            "dead_number": self.dead_number,
            "injured_number": self.injured_number,
            "car_number": self.car_number,
            "member_number": self.member_number,
            "road_network_disadvantages": self.road_network_disadvantages,
            "driving_mode_factors": self.driving_mode_factors,
            "weather": self.weather,
            "roadway_condition": self.roadway_condition,
            "lighting": self.lighting,
            "longitude": self.longitude,
            "latitude": self.latitude,
            "direct_traffic_violations": self.direct_traffic_violations,
        }
    
class AccidentList:
    def __init__(self, content: list):
        self.accident_list = [Accident(document) for document in content]
        # for document in content:
        #     self.accident_list.append()

    def get(self):
        return [accident.get() for accident in self.accident_list]
    
    def get_df(self):
        return pd.json_normalize(self.get())
    
    def extend(self, content: list):
        self.accident_list.extend([Accident(document) for document in content])